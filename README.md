# Exercice d'autoformation WordPress

## Description
- Développer un site d'inscriptions à des événements en utilisant wp-cubi (la stack WordPress de Globalis).

## Temps estimé
- 5 jours de travail

## Expression de besoin

Le site doit permettre de gérer des inscriptions à des événements.

Il faut pouvoir créer / modifier / supprimer des événements dans l'administration, ils sont définis par :
- Un titre
- Une adresse (texte)
- Une date
- Un texte de présentation
- Un visuel
- Un nombre de places disponibles (ou illimité)
- Un fichier PDF "billet d'entrée"

En front, on doit pouvoir consulter la fiche d'un évènement, ainsi qu'un formulaire d'inscription à l'événement, avec les informations suivantes :
- Nom
- Prénom
- Email
- Date de naissance
- Je suis : lycéen/étudiant - salarié - retraité - chômeur - autre

Une fois l'inscription effectuée, l'utilisateur doit pouvoir télécharger le fichier PDF "billet d'entrée" qui lui permettra d'accéder à l'événement.

Il doit également recevoir un accusé de réception par mail, avec le billet PDF en pièce-jointe.

Les inscriptions effectuées doivent être consultables dans l'administration.

Il faut pouvoir visualiser également dans l'administration, sur la liste des événements, le nombre d'inscrits / sur le nombre de places disponibles.

S'il n'y a plus de place disponible pour un événement, les inscriptions à cet événement doivent être impossibles.

Saisir des informations au mauvais format doit être impossible (exemple : mauvais format d'email ou de date), et le formulaire doit être sécurisé.

En front, le site doit contenir les pages suivantes :
- Page d'accueil (liste des 3 derniers évenements + bouton "voir la liste de tous les événements")
- Liste des événements
- Fiche d'un événement + formulaire
- Page générique de texte / contenu (exemple : Qui sommes-nous ?)
- Page mentions légales avec un template spécifique

Un menu (utilisant le système de menus de WordPress) doit permettre d'accéder à la page d'accueil, la page liste, et la page mentions légales.

Fournir également un template pour les erreurs 404.

Si le temps le permet, ajouter les fonctionnalités suivantes :

- Dans le back-office, bouton d'export excel des inscrits à un évènement
- Tache planifiée : tous les soirs, envoyer par mail à l'administrateur les exports excels des 3 derniers évènements
- Encapsuler les événements et les inscriptions dans une couche d'abstraction POO

## Ressources obligatoires à utiliser

- Pour initialiser le projet : [wp-cubi](https://github.com/globalis-ms/wp-cubi)
- Pour déclarer les post types : [johnbillion/extended-cpts](https://github.com/johnbillion/extended-cpts)
- Pour les champs personalisés en back-office : [ACF (advanced-custom-fields)](https://wordpress.org/plugins/advanced-custom-fields/)


## Livrable attendu

À la fin de l'exercice, envoyer par mail au correcteur :
- Tout ce qui permet de mettre le site en production, c'est à dire :
- Dépôt git d'installation du projet et de ses dépendances
- Dump de la BDD (et des uploads associés, s'il y en a)
- README.md si besoin

## Notions à maitriser d'ici la fin de l'exercice

- Installation de `wp-cubi`
- Utilisation des hooks WordPress (`add_action()`, `add_filter()`)
- Déclaration de `custom post types`
- Ajout de champs personnalisés en back-office / utilisation d'`ACF`
- Ajouter des colonnes sur les vues listes du back-office
- Theming de base, système de templating de WordPress, menus WordPress
- Ajout de feuilles de styles css et scripts js à un frontend WordPress (`wp_enqueue_style()`, `wp_enqueue_script(`)
- Constructions de requêtes avec `WP_Query`
- Utilisation de la `loop` WordPress
- Tâches planifiées WordPress (`wp-cron`)
- Utilisation de `wp_mail()`

## Documentation (non exhaustif, google est ton ami)

- https://developer.wordpress.org/themes/basics/
- https://codex.wordpress.org/Theme_Development
- https://yoast.com/wordpress-theme-anatomy/
- https://capitainewp.io/formations/developper-theme-wordpress/

## Remarques sur l'exercice

- CSS/JS : Ce n'est pas un exercice d'intégration html/css ou de javascript, donc ne pas perdre de temps sur la qualité graphique du front, il faut juste que ca affiche les bonnes informations et que ca fonctionne. Il n'y a pas de maquettes fournies pour la même raison. Du pré-processing (SASS/SCSS) peut être utilisé mais ce n'est pas obligatoire.
- Créer le thème (ne pas utiliser de thème existant). Mettre l'ensemble du code nécessaire dedans (pas la peine de créer des plugins).
- Si l'exercice est terminé et qu'il reste du temps de formation : apprendre à utiliser [wp-cubi-robo-globalis](https://github.com/globalis-ms/wp-cubi-robo-globalis) pour build les assets du theme. À noter que ce package n'est pas encore documenté, mais tous les devs WordPress du plateau peuvent aider à sa prise en main.
- Mis à part les extensions (plugins) WordPress inclues dans wp-cubi, et advanced-custom-fields, aucune extension WordPress ne doit être utilisée. Il est par contre autorisé (mais pas obligatoire) d'utiliser des biliothèques PHP via composer, si leur choix est justifié et maîtrisé.
- Les formulaires en front doivent être développés manuellement (ET PAS AVEC advanced-custom-fields, qu'on utilise uniquement dans le back-office).
